import newick3,phylo3,os,sys
import collections

MIN_TAXA = 8

"""
the pattern for the tip names is taxon_name@seq_name
"""

OUTGROUP = "9"

#if taxon id pattern changes, change it here
def get_name(name):
	return name.split("@")[0]
	
def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_back_labels(node,root): #no repeat
	all_labels = get_front_labels(root)
	front_labels = get_front_labels(node)
	return set(all_labels) - set(front_labels)
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

def get_back_names(node,root): #may include duplicates
	back_labels = get_back_labels(node,root)
	return [get_name(i) for i in back_labels]

def get_dup_names(names): #remove uniq names and leave only duplicated names
	uniq_names = [x for x,y in collections.Counter(names).items() if y == 1]
	dup_names = []
	for name in names:
		if name not in uniq_names:
			dup_names.append(name)
	return dup_names

#calculate now many dup taxa will be reduced from cutting
def get_dup_reduction(node,curroot)
	front_names = get_front_names(node)
	front_dup_names = get_dup_names(front_names)
	back_names = get_back_names(node,curroot)
	back_dup_names = get_dup_names(back_names)
	return len(all_dup_names) - len(front_dup_names) - len(back_dup_names)

#count how many taxa in front of a node
def count_taxa(node):
	return len(set(get_front_names(node)))
	
#check whether there is any duplicated taxon names in front
#and how many total taxon names in front
def get_front_score(node):
	names = get_front_names(node)
	return len(names) == len(set(names)), len(set(names))

#check whether there is any duplicated taxon names in back
#and how many total taxon names in back
def get_back_score(node,root):
	back_names = get_back_names(node,root)
	return len(back_names) == len(set(back_names)), len(set(back_names))

#smooth the kink created by prunning
#to prevent creating orphaned tips after prunning twice at the same node
def remove_kink(node,curroot):
	if node == curroot and curroot.nchildren == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: curroot = phylo3.reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

#the side to be prunned away can only have taxa in the other side
#or taxa not found anywhere else on the tree other than the two sides of the mrca
def check(root,n1,n1_direction,n2,n2_direction):
	if n1_direction == "front":
		n1_labels = set(get_front_labels(n1))
	else: n1_labels = set(get_back_labels(n1,root))
	if n2_direction == "front":
		n2_labels = set(get_front_labels(n2))
	else: n2_labels = set(get_back_labels(n2,root))
	all_labels = set(get_front_labels(root))
	out_labels = all_labels - n1_labels - n2_labels
	out_name_set = set([get_name(i) for i in out_labels])
	n1_name_set = set([get_name(i) for i in n1_labels])
	n2_name_set = set([get_name(i) for i in n2_labels])
	if len(n1_name_set) > len(n2_name_set):
		larger,smaller = n1,n2
		larger_side_name_set,smaller_side_name_set = n1_name_set,n2_name_set
	else:
		larger,smaller = n2,n1
		larger_side_name_set,smaller_side_name_set = n2_name_set,n1_name_set
	cut_larger,cut_smaller = True,True
	for name in smaller_side_name_set:
		if name in out_name_set:
			cut_smaller = False
			break
	if cut_smaller:	return smaller
	for name in larger_side_name_set:
		if name in out_name_set:
			cut_larger = False
			break
	if cut_larger: return larger
	return None

#takes a scoring array for front or back and then takes the node
def prune(cut_front,node,root,outfile):
	if cut_front == True: #front, simple
		if len(node.leaves()) > 3:
			outfile.write(newick3.tostring(node)+";\n")
		kink = node.prune()
		if len(root.leaves()) > 3:
			newnode,root = remove_kink(kink,root)
		return root
	else: #back, need to prune the root and save the front
		if node != root:
			par = node.parent
			par.remove_child(node)
		node.prune()
		if len(root.leaves()) > 3:
			outfile.write(newick3.tostring(root)+";\n")
		if len(node.leaves()) > 3:
			node,newroot = remove_kink(node,node)
		else: newroot = node
		return newroot
		
def prune_from_tip(curroot):
	scDICT = {} #key is node object, value is a list
	#[node][0],[node][1] = front_dupe,back_dupe
	#[node][2],[node][3] = num_front_taxa,num_back_taxa
	for node in curroot.iternodes(): #count front and back scores
		front_dup,num_front_taxa = get_front_score(node)
		back_dup,num_back_taxa = get_back_score(node,curroot)
		scDICT[node] = [front_dup,back_dup,num_front_taxa,num_back_taxa]
	
	smallest_score = 100000000
	smallest_node = None
	for node in curroot.iternodes():
		if node.istip: continue #skip tips and only check internal nodes
		#the largest one of the three clades is considered to be the "root" for now
		if node == curroot: #check the root
			largest,n1,n2 = curroot.children[0],curroot.children[1],curroot.children[2]
			if scDICT[largest][2] < scDICT[n1][2]: #compare front scores
				largest,n1 = n1,largest
			if scDICT[largest][2] < scDICT[n2][2]:
				largest,n2 = n2,largest
			#now largest is the largest of the three root nodes.
			if scDICT[n1][0]==True and scDICT[n2][0]==True and scDICT[largest][1]== False:
				print "Checking root",scDICT[largest],scDICT[n1],scDICT[n2]
				cut_node = check(curroot,n1,"front",n2,"front")
				if cut_node != None and scDICT[cut_node][2]<smallest_score and scDICT[cut_node][2] > 0:
					smallest_score = scDICT[cut_node][2]
					smallest_node = cut_node
					cut_front = True
					if smallest_score == 1: break #smallest possible is 1. Stop checking
					print "updating smallest score",scDICT[cut_node],cut_front			

		#consider each node as a trifurcating point with two front and one back clades
		elif scDICT[node][3] >= max(scDICT[node.children[0]][2],scDICT[node.children[1]][2]): 
			#back is the largest clade
			n1,n2 = node.children[0],node.children[1]
			if scDICT[node][0]==False and scDICT[n1][0]==True and scDICT[n2][0]==True:
				#dup in front but not within either one of the two children
				cut_node = check(curroot,n1,"front",n2,"front")
				if cut_node !=None and scDICT[cut_node][2]<smallest_score and scDICT[cut_node][2] > 0:
					print "Checking front",scDICT[node],scDICT[n1],scDICT[n2]
					smallest_score = scDICT[cut_node][2]
					smallest_node = cut_node
					cut_front = True
					if smallest_score == 1: break #smallest possible is 1. Stop checking
					print "updating smallest score",scDICT[cut_node],cut_front

		else: #one of the children is the largest clade. Either cut the sister or cut the parent
			if scDICT[node.children[0]][2] > scDICT[node.children[1]][2]:
				largest,n1 = node.children[0],node.children[1]
			else: n1,largest = node.children[0],node.children[1]
			n2 = node #direction of n2 is "back". This should be node instead of node.parent!
			if scDICT[largest][1] == False and scDICT[n1][0] == True and scDICT[n2][1] == True:
				print "Checking back",scDICT[largest],scDICT[n1],scDICT[n2]
				cut_node = check(curroot,n1,"front",n2,"back")
				if cut_node == n1: #cut front
					if scDICT[n1][2]<smallest_score and scDICT[n1][2]>0: 
						smallest_score = scDICT[n1][2]
						smallest_node = n1
						cut_front = True
						if smallest_score == 1: break #smallest possible is 1. Stop checking
						print "updating smallest score",scDICT[n1],cut_front
				elif cut_node == n2: #cut back
					if scDICT[n2][3]<smallest_score and scDICT[n2][3]>0:
						smallest_score = scDICT[n2][3]
						smallest_node = n2
						cut_front = False
						if smallest_score == 1: break #smallest possible is 1. Stop checking
						print "updating smallest score",scDICT[n2],cut_front
						print "smallest node",scDICT[smallest_node],cut_front
	if smallest_node != None:
		print "smallest node",scDICT[smallest_node]
		if smallest_node.istip: print smallest_node.label
		elif cut_front == True:
			print "Cut from front",get_front_labels(smallest_node)
		else: print "Cut from back",get_back_labels(smallest_node,curroot)
	else: cut_front = None
	return smallest_node,cut_front



#make the cut that reduce duplicated names the most and is also the longest branch
def prune_from_middle(curroot):
	#find the branch that reduces taxon duplication the most
	#and the longest branch
	longest_brlen = 0
	highest_dup_reduction = 0
	all_names = get_front_names(curroot)
	all_dup_names = get_dup_names(all_names)
	for node in curroot.iternodes():
		if node != curroot:
			highest_dup_reduction = max(get_dup_reduction(node,curroot),highest_dup_reduction)
	print "The longest branch length is",longest_brlen
	print "The highest dup reduction is",highest_dup_reduction
	
	#check for match between highest reduction vs. longest branch
	for node in curroot.iternodes():
		if node == curroot: continue
		if node.length == longest_brlen:
			front_names = get_front_names(node)
			front_dup_names = get_dup_names(front_names)
			back_names = get_back_names(node,curroot)
			back_dup_names = get_dup_names(back_names)
			dup_reduction = len(all_dup_names) - len(front_dup_names) - len(back_dup_names
			if dup_reduction == highest_dup_reduction:
				par = cut_node.parent
				par.remove_child(cut_node)	
				cut_node.prune()
				if len(cut_node.leaves()) > 3:
					newnode,front_tree = remove_kink(cut_node,cut_node)
				else: front_tree = None
				if len(curroot.leaves()) > 3:
					newnode,back_tree = remove_kink(par,curroot)
				else: back_tree = None
				return front_tree,back_tree
	return None #

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python prune_paralogs.py DIR >log"
		sys.exit(0)
		
	DIR = sys.argv[1]+"/"
	for treefile in os.listdir(DIR):
		if treefile[-9:] != ".homoTree": continue
		print treefile
		with open(DIR+treefile,"r") as infile: #only 1 tree in each file
			intree = newick3.parse(infile.readline())
		clusterID = treefile.split(".")[0]

		#decide whether tree has enough taxa
		if count_taxa(intree) < MIN_TAXA:
			print "Less than "+str(MIN_TAXA)+"ingroup taxa"
			continue
		
		#write the same tree out if no dup names 
		if len(get_dup_names(intree)) == 0:
			print "no duplicated taxon names"
			with open(DIR+clusterID+".ortho1","w")
				outfile.write(newick3.tostring(intree)+";\n")
			continue
		
		#Check outgroup monophyly and reroot
		out_monophyly = True
		lvs = intree.leaves()
		outgroup_matches = {} #key is taxon name, value is the tip node object
		curroot = intree
		outgroup_labels = []
		for leaf in lvs: #get all the outgroup labels and tips on the tree
			name = get_name(leaf.label)
			if name == OUTGROUP:
				outgroup_matches[name] = leaf
				outgroup_labels.append(leaf.label)
		if outgroup_labels == []:
			#print "No outgroup in the tree. Only prune from middle"
			continue
		elif len(outgroup_labels) == 1: #one single outgroup
			#cannot reroot on a tip so have to go one more node into the ingroup
			out_mrca = outgroup_matches[get_name(outgroup_labels[0])].parent
			curroot = phylo3.reroot(curroot,out_mrca)
			"""
			Prune from the tip
			"""
			with open(outDIR+treefile+".reroot","w") as outfile:
				outfile.write(newick3.tostring(tree)+";\n")
		else: #has multiple outgroup labels. Check monophyly and reroot
			newroot = None
			for node in curroot.iternodes():
				if node == curroot: continue #skip the root
				front_names = get_front_names(node)
				back_names = get_back_names(node,curroot)
				front_in_names,front_out_names,back_in_names,back_out_names = 0,0,0,0
				for i in front_names:
					if i in OUTGROUPS: front_out_names += 1
					else: front_in_names += 1
				for j in back_names:
					if j in OUTGROUPS: back_out_names += 1
					else: back_in_names += 1
				if front_in_names==0 and front_out_names>0 and back_in_names>0 and back_out_names==0:
					#ingroup at back, outgroup in front
					newroot = node
					break
				if front_in_names>0 and front_out_names==0 and back_in_names==0 and back_out_names>0:
					#ingroup in front, outgroup at back
					newroot = node.parent
					break
			if newroot != None:
				tree = phylo3.reroot(curroot,newroot)
				with open(outDIR+treefile+".reroot","w") as outfile:
					outfile.write(newick3.tostring(tree)+";\n")
		
		
		#now start pruning
		trees = [intree]
		while True:
			newtrees = []
			for tree in trees:
				curroot = tree
				while True: #cut from the tip iteratively
					cut_node,cut_front = prune_from_tip(curroot)
					if cut_node != None: #tip cuts were found
						curroot = prune(cut_front,cut_node,curroot,outfile)
						dup_name_set = set(get_dup_names(curroot))
						print "duplicated names left",dup_name_set
						if dup_name_set == [] or count_taxa(curroot) <= MIN_TAXA:
							#cutting from tip finished all the cuttings needed
							#write the rest and finish with this tree
							outfile.write(newick3.tostring(curroot)+";\n")
							break
					else: break #need to continue cutting big chunks
				if get_dup_names(curroot) != [] and count_taxa(curroot) > MIN_TAXA:
					print "cut large chunk"
					front_tree,back_tree = cut_large_chunk(curroot)
					if front_tree != None:
						if get_dup_names(front_tree)!=[] and count_taxa(front_tree)>=MIN_TAXA:
							newtrees.append(front_tree) #save for the next round
						else: outfile.write(newick3.tostring(front_tree)+";\n") #finish this tree
					if back_tree != None:
						if get_dup_names(back_tree)!=[] and count_taxa(back_tree)>=MIN_TAXA:
							newtrees.append(back_tree) #save for the next round
						else: outfile.write(newick3.tostring(back_tree)+";\n") #finish this tree
			if newtrees == []: break
			else: trees = newtrees
		outfile.close()
