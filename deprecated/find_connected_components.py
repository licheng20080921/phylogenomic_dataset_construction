"""
input .edges file
output file with edges in each connected components
"""

import os,sys
import networkx as nx

MIN_TAXA = 4

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python find_connected_components.py edges outDIR"
		sys.exit()
	
	print "Reading edges"
	infile = open(sys.argv[1],"r")
	G = nx.Graph() #initiate the graph
	for line in infile:
		if len(line) > 3:
			spls = line.strip().split("\t")
			G.add_edge(spls[0],spls[1])
	infile.close()
	
	print "Writing connected component and edge files"
	ccs = nx.connected_components(G) #get the connected components
	outfile1 = open(sys.argv[1]+".ccs","w")
	DIR = sys.argv[2]+"/"

	cc_count = 0	
	for cc in ccs: #cc is a list of seqids in each connected component
		SG = G.subgraph(cc)
		if len(cc) >= MIN_TAXA:
			cc_count += 1
			outfile1.write("cc"+str(cc_count)+":")
			for seqid in cc: outfile1.write(seqid+"\t")
			outfile1.write("\n")
			outfile = open(DIR+"cc"+str(cc_count)+".edges","w")
			for edge in SG.edges(): #an "edge" is stored as a tuple
				query,hit = edge[0],edge[1]
				outfile.write(query+"\t"+hit+"\n")
			outfile.close()
	outfile1.close()



